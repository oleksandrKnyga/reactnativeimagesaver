/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
var React = require('react-native');
var Main = require('./app/components/Main');
var {
  AppRegistry,
  StyleSheet,
  NavigatorIOS
} = React;

var styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: '#111111'
  },
});

class FileStorage extends React.Component {
  render() {
    return (
      <NavigatorIOS
        style={styles.container}
        initialRoute={{
          title: 'Images Saver',
          component: Main
        }}></NavigatorIOS>
    );
  }
}

AppRegistry.registerComponent('FileStorage', () => FileStorage);
