var React = require('react-native');
var {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	StyleSheet
} = React;

const styles = StyleSheet.create({
	button: {
		
	},
	text: {
		color: '#000'	
	}
});

class Press extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			myButtonOpacity: 1
		};
	}
	render() {

	  return (
	    <TouchableOpacity onPress={() => this.setState({myButtonOpacity: 0.5})}
	                      onPressOut={() => this.setState({myButtonOpacity: 1})}>
	      <View style={[styles.button, {opacity: this.state.myButtonOpacity}]}>
	        <Text style={styles.text}>{this.props.rowData.name}</Text>
	      </View>
	    </TouchableOpacity>
	  )
	}
}

module.exports = Press;