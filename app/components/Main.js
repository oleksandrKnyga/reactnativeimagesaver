var React = require('react-native');
var Press = require('./Press');
var Button = require('react-native-button');
var mfs  = require('../utils/fs');

var {
	View,
	Text,
	StyleSheet,
	TextInput,
	TouchableHighlight,
	ActivityIndicatorIOS,
	ListView,
	ScrollView,
	Image
} = React;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    marginTop: 65,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#48BBEC'
  },
  list: {
  	flex: 1
  },
  row: {
  	flex: 1,
  	flexDirection: 'row',
  	borderBottomWidth: 1,
  	padding: 5
  },
  idCell: {
  	flex: 1,
  	alignSelf: 'stretch',
  },
  idCellValue: {
  	flex: 1,
  	alignSelf: 'stretch',
  },
  nameCell: {
  	flex: 1,
  	alignSelf: 'stretch',
  },
  nameCellValue: {
  	flex: 1,
	alignSelf: 'stretch',
  },
  centering: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

class Main extends React.Component {
	constructor(props) {
		super(props);

		var ds = new ListView.DataSource({
			rowHasChanged: (r1, r2) => r1 !== r2
		});

		var rows = [{id: 1, name: 'aaa 1'}];

		this.fields = {
			input: null
		};

		this.state = {
			animating: true,
			images: []
		};

		this.updateImages();
	}
	updateImages() {
		mfs.getFiles().then((images) => {
			console.log(images);
			this.setState({
				animating: false,
				images: images || []
			});
		});
	}
	addImage(url) {
		this.setState({
			animating: true,
		});

		mfs.downloadFile(url)
			.then(() => {
				this.setState({
					animating: false
				});

				this.updateImages();
			});
	}
	_handlePress() {
		if(/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test(this.fields.input)) {
			this.addImage(this.fields.input);
		}

		this.refs.input.setNativeProps({text: ''});
	}
	render() {
		var images = this.state.images.map((image, i) => 
			(<Image
				key={i}
		        style={{minHeight: 90, width:90}}
		        source={{uri: image}} />)
		);

		return (
	      <View style={styles.container}>
	      	  <TextInput
	      	    ref="input"
	      	    style={{height: 40, backgroundColor: '#ffffff'}}
	      	    onChangeText={(text) => this.fields.input = text}
	      	  />
	      	  <Button 
	      	  	style={{fontSize: 20, color: 'green', backgroundColor: '#eeeeee', padding: 10, marginBottom: 20}}
	      	  	onPress={this._handlePress.bind(this)}>
	      	  	Add image
	      	  </Button>
	      	  <View style={{flexDirection: 'row'}}>
	      	  	{images}
	      	  </View>
	          <ActivityIndicatorIOS
	          	animating={this.state.animating}
	            size="large"
	            color="#aa3300"
	          />
	      </View>
		);
	}
}

module.exports = Main;