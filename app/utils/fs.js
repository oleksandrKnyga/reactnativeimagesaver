// require the module
var RNFS = require('react-native-fs');
var FileDownload = require('react-native-file-download');

module.exports = {
  downloadFile: function(url) {
    return FileDownload.download(url, RNFS.DocumentDirectoryPath);
  },
  getFiles: function() {
    // get a list of files and directories in the main bundle
    return RNFS.readDir(RNFS.DocumentDirectoryPath)
      .then((result) => {
        var img = result.filter((item) => /\.(png|svg|jpg|jpeg|gif)$/ig.test(item.path));

        if(img && img.length > 0) {
          return img.map((image) => image.path);
        }

        return null;
      })
  }
}